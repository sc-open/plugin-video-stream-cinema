# plugin.video.stream-cinema
kodi plugin pre stream-cinema.online a kompatibilné servery

Verzia otvorená pre všetky zdroje dát zachovávajúce protokol pôvodnej verzie.

## Zraniteľnosti a ich riešenie

Kodi je otvorený systém a poskytuje platformu pre doplnky. Doplnky sa navzájom využívajú a od hlavných autorov
si navzájom poskytujú priestor na ďalšie rozširovanie. 

Aj tento doplnok využíva množstvo cudzej práce, bez ktorej by nebol možný. 
V porovnaní s tým na čom je závislý je len pár riadkami Python kódu, ktoré boli užívateľom venované na úžívanie
pod GNU GPLv3 licenciou. Táto licencia nedáva autorom právo na neskoršiu ľubovôlu a diskrimináciu voči jeho užívateľom.    
 
Kodi je vo svojom jadre bezpečné, no ekosystém addonov vyžaduje maximálnu dôveru, k autorovi ako aj správcom
repozitárov. 

Žiaľ, originálna verzia tohto addonu sa stala ukážkovým príkladom toho, čoho sa treba vo svete IT obávať a prečo
je nutné vždy vyžadovať maximálnu otvorenosť, transparentnosť a auditovateľnosť software a osobných informácii. 
A to rovnako v zábavnom priemysle, ako aj v pri spravovaní informácíí o našom zdravotnom stave, školských výsledkoch, 
nákupných zvyklostiach a pohybe.

Tento klon vytvorený na základe princípov GNU GPLv3 licencie je určený na zvýšenie povedomia, 
bezpečia a slobody užívateľov tak, ako možno autor doplnku pôvodne zamýšľal. 
   
### Zneužiteľnosť xbmc.getCondVisibility na diskrimináciu uživateľov

Protokol SC umožňuje podmieňovať zobrazenie niektorých záznaov menu vyhodnotením podmienky prostredníctvom
funkcie zabudovanej priamo v jadre [Kodi](https://kodi.wiki/view/List_of_boolean_conditions). 
Toto je samo o sebe bezpečné, ale umožnuje diskrimináciu užívateľov napr. podľa nainštalovaných iných doplnkov.  
  
Pri nedôveryhodných autoroch databáz, ktorý zneužívajú otvorenosti Kodi 
a obmedzujú vo funkcionalite užívateľov, je možné využiť nastavenie `Ignorovať pri kontrole viditeľnosti`. 

Hodnota by mala byť Python kompatibilný regex, napr. `context\.webshare`. 

Technicky je to aplikované cca. nasledovne: `re.sub(re.compile(cisar_visibility_filter, re.IGNORECASE), '', visible)`  

## Vzdialená konfigurácia

| Názov | Default | Popis |
|-------|---------|-------|
| cisar_source | http://stream-cinema.online/kodi | cesta k serveru komunikujúcemu kompatibilným protokolom |
| cisar_bug_submit |  | bbaron si posielal chyby na svoj server (vhodné pre vývojárov) |
| cisar_source_search |  | ak chceme použiť iný server pre odpovede vyvolané akciou `csearch` |
| sock5 |  | SOCK5 proxy adresa a port (napr. 127.0.0.1:9050) |
| usecache | true | ak je vypnuté preťažuje to zdroj dát pri každej zmene obrazovky (len pre vývojárov) |
| cisar_visibility_filter |   | regex substitúcia, ktorá sa aplikuje na podmienku zobrazenia záznamu |
      
### Pôvodné stream-cinema.online s HTTPS protokolom

```json
{
  "cisar_source":"https://stream-cinema.online/kodi",
  "cisar_source_search": "",
  "cisar_bug_submit":"",
  "usecache":true,
  "sock5":"",
  "cisar_visibility_filter":"context\\.webshare"
}
```

### Pôvodné stream-cinema.online, ale cez Tor proxy

```json
{
  "cisar_source":"https://stream-cinema.online/kodi",
  "cisar_source_search": "",
  "cisar_bug_submit":"",
  "usecache":true,
  "sock5": "localhost:9050",
  "cisar_visibility_filter":"context\\.webshare"
}
```

### Pôvodné stream-cinema.online aj s odosielaním bugov

```json
{
  "cisar_source":"https://stream-cinema.online/kodi",
  "cisar_source_search": "",  
  "cisar_bug_submit":"http://movies.bbaron.sk/plugin/submit/",
  "usecache":true,
  "sock5":"",
  "cisar_visibility_filter":""
}
```

### Vlastný lokálny zdroj dát s odlišným zdrojom pre csearch akciu

```json
{
  "cisar_source":"https://localhost:3000/kodi",
  "cisar_source_search": "https://localhost:3001/",
  "cisar_bug_submit":"",
  "usecache":true,
  "sock5":"",
  "cisar_visibility_filter":""
}
```
